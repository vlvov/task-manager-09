package ru.t1.vlvov.tm.repository;

import ru.t1.vlvov.tm.api.ICommandRepository;
import ru.t1.vlvov.tm.constant.ArgumentConst;
import ru.t1.vlvov.tm.constant.TerminalConst;
import ru.t1.vlvov.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    public static Command ABOUT = new Command(TerminalConst.ABOUT, ArgumentConst.ABOUT,"Show developer info.");

    public static Command EXIT = new Command(TerminalConst.EXIT, null, "Close application.");

    public static Command HELP = new Command(TerminalConst.HELP, ArgumentConst.HELP, "Show application commands.");

    public static Command VERSION = new Command(TerminalConst.VERSION, ArgumentConst.VERSION, "Show application version.");

    public static Command INFO = new Command(TerminalConst.INFO, ArgumentConst.INFO, "Show available memory.");

    public static Command COMMANDS = new Command(TerminalConst.COMMANDS, ArgumentConst.COMMANDS, "Show commands.");

    public static Command ARGUMENTS = new Command(TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS, "Show arguments.");

    private static Command[] TERMINAL_COMMANDS = new Command[] {
        ABOUT, EXIT, HELP, VERSION, INFO, COMMANDS, ARGUMENTS
    };

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
