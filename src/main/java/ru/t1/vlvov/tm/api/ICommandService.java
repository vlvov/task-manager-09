package ru.t1.vlvov.tm.api;

import ru.t1.vlvov.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
